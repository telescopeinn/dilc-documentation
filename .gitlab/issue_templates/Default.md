### Setup Information

- Legacy [ ] or v2 [ ] hardware? 
- Which software version are you using (bottom right of the dashboard)? 

### What Happened

Please describe the issue in as much detail as possible. 

- Is the issue software-, hardware-related, or not known? 
- What happened? 
- What did you expect to happen instead? 
- If there are screenshots that help to illustrate the issue, please include them (they can be pasted directly into the issue).

### Resolution Attempts

Did you try any resolution attempts? What was/were the result?

### Importance

We treat any user-submitted issues with the highest priority, but please indicate how critical resolution of this issue 
is to your workflow. 
